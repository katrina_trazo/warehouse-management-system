<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {
   //
    protected $fillable=[
        'id',
        'supplier_name',
        'contact_no',
        'email_add',
        'address'
    ];
}
