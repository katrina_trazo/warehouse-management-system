<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App\Supplier;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $suppliers=Supplier::all();
        return view('suppliers.index',compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'supplier_name' => 'required',
        'contact_no' => 'required',
        'email_add' => 'required',
        'address' => 'required'
        ]);

        $supplier=$request->all();
        Supplier::create($supplier);
        Session::flash('flash_message', 'Supplier successfully added!');
        return redirect('suppliers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $supplier = Supplier::find($id);
        return View('suppliers.show')
            ->with('supplier', $supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
    
        $supplier=Supplier::findOrFail($id);
        return view('suppliers.edit')->withSupplier($supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $supplier=Supplier::findOrFail($id);

         $this->validate($request, [
        'supplier_name' => 'required',
        'contact_no' => 'required',
        'email_add' => 'required',
        'address' => 'required'
        ]);

        $input = $request->all();
        $supplier->fill($input)->save();
        Session::flash('flash_message', 'Supplier successfully updated!');
        return redirect('suppliers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Supplier::find($id)->delete();
        Session::flash('flash_message', 'Supplier successfully deleted!');
        return redirect('suppliers');
    }
}
