@extends('layout.template')

@section('content')
    <h1>Add Supplier</h1>
    @if($errors->any())
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    @if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
    @endif
    
    {!! Form::open(['url' => 'suppliers']) !!}
    <div class="form-group">
        {!! Form::label('Supplier Name', 'Supplier Name:') !!}
        {!! Form::text('supplier_name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Contact Number', 'Contact Number:') !!}
        {!! Form::text('contact_no',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Email Address', 'Email Address:') !!}
        {!! Form::text('email_add',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Address', 'Address:') !!}
        {!! Form::text('address',null,['class'=>'form-control']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
@stop