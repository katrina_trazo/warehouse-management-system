@extends('layout.template')

@section('content')
    <h1>Edit Supplier Information</h1>
    {!! Form::model($supplier,['method' => 'PATCH','route'=>['suppliers.update',$supplier->id]]) !!}
    <div class="form-group">
        {!! Form::label('Supplier Name', 'Supplier Name:') !!}
        {!! Form::text('supplier_name',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Contact Number', 'Contact Number:') !!}
        {!! Form::text('contact_no',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Email Address', 'Email Address:') !!}
        {!! Form::text('email_add',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Address', 'Address:') !!}
        {!! Form::text('address',null,['class'=>'form-control']) !!}
    </div>
   
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop