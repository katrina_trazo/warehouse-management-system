@extends('layout/template')

@section('content')
 <h1>Mitsukoshi Motors Phils. Inc.</h1>
 <a href="{{url('/suppliers/create')}}" class="btn btn-success">Add Suppliers</a>
 <hr>
 <table class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Id</th>
         <th>Name</th>
         <th>Contact Number</th>
         <th>Email Address</th>
         <th>Address</th>
         <th colspan="3">Actions</th>
     </tr>
     </thead>
     <tbody>
     @foreach ($suppliers as $supplier)
         <tr>
             <td>{{ $supplier->id }}</td>
             <td>{{ $supplier->supplier_name }}</td>
             <td>{{ $supplier->contact_no }}</td>
             <td>{{ $supplier->email_add }}</td>
             <td>{{ $supplier->address }}</td>
             <td><a href="{{route('suppliers.show' , $supplier->id) }}" class="btn btn-info">View</a></td>
             <td><a href="{{route('suppliers.edit',$supplier->id)}}" class="btn btn-warning">Edit</a></td>
             <td>
             {!! Form::open(['method' => 'DELETE', 'route'=>['suppliers.destroy', $supplier->id]]) !!}
             {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
             {!! Form::close() !!}
             </td>
         </tr>
     @endforeach

     </tbody>

 </table>
@endsection