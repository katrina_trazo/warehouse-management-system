@extends('layout.template')

@section('content')
    <h1>Supplier Information</h1>

    <form class="form-horizontal">
        <div class="form-group">
            <label>Supplier Name</label>
            <div class="col-sm-10">
                <p class="form-control"> {{$supplier->supplier_name}}</p>
            </div>
        </div>
        <div class="form-group">
            <label>Contact Number</label>
            <div class="col-sm-10">
                <p class="form-control"> {{$supplier->contact_no}}</p>
            </div>
        </div>
        <div class="form-group">
            <label>Email Address</label>
            <div class="col-sm-10">
                <p class="form-control"> {{$supplier->email_add}}</p>
            </div>
        </div>
        <div class="form-group">
            <label>Address</label>
            <div class="col-sm-10">
                <p class="form-control">{{$supplier->address}}</p>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="{{ url('suppliers')}}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </form>
@stop